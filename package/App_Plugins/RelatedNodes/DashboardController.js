﻿angular.module('umbraco').controller('Our.Umbraco.RelatedNodes.DashboardController', ['$scope', '$http', 'Our.Umbraco.RelatedNodes.Service', 'notificationsService', 'dialogService',
    function ($scope, $http, relatedNodesService, notificationsService, dialogService) {

        $scope.loading = true;
        $scope.unlinkedNodes = [];
        $scope.currentSort = 'name';
        $scope.currentSortAscending = true;
        $scope.pageSize = 25;
        $scope.currentPage = 1;
        $scope.totalPages = 0;
        $scope.sortedNodes = [];
        $scope.filteredNodes = [];
        $scope.hasPrevious = true;
        $scope.hasNext = true;
        $scope.hasChecked = false;
        $scope.excludedContentFolders = Umbraco.Sys.ServerVariables.RelatedNodes.DefaultExcludedContentFolderIds || '';
        $scope.excludedMediaFolders = Umbraco.Sys.ServerVariables.RelatedNodes.DefaultExcludedMediaFolderIds || '';
        $scope.includeWithOutgoing = Umbraco.Sys.ServerVariables.RelatedNodes.DefaultIncludeNodesWithOnlyOutgoingLinks ? true : false;
        $scope.checkedCount = 0;
        $scope.actions = [
            { name: "Publish", alias: "publish", icon: "globe" },
            { name: "Unpublish", alias: "unpublish", icon: "remove" },
            { name: "Export", alias: "export", icon: "out" },
            { name: "Move", alias: "move", icon: "enter" },
            { name: "Delete", alias: "delete", icon: "delete" }
        ];

        $scope.refresh = function () {
            $scope.loading = true;
            relatedNodesService.getUnlinkedNodeData($scope.excludedContentFolders + ',' + $scope.excludedMediaFolders, $scope.includeWithOutgoing)
                .then(
                    function (data) {
                        $scope.loading = false;
                        if (data.error) {
                            notificationsService.error('Refresh Error', 'Unable to load the unlinked nodes from the database');
                            console.log("An error occurred while trying to process the API call - " + data.error);
                            return;
                        }

                        var nodes = data.data || data.Data || [];
                        for (var i = 0; i < nodes.length; i++) nodes[i] = new LinkedNodeData(nodes[i]);

                        $scope.unlinkedNodes = nodes;
                        $scope.hasChecked = false;
                        $scope.checkedCount = 0;
                        $scope.totalPages = parseInt(nodes.length / $scope.pageSize) + ((nodes.length % $scope.pageSize) === 0 ? 0 : 1);
                        if ($scope.currentPage > $scope.totalPages) $scope.currentPage = $scope.totalPages;
                        if ($scope.currentPage < 1) $scope.currentPage = 1;

                        $scope.sortNodes();
                    },
                    function (error) {
                        $scope.loading = false;
                        console.log("An error occurred while trying to process the API call - " + JSON.stringify(error));
                        notificationsService.error('Refresh Error', 'Unable to load the unlinked nodes from the database');
                    }
                );
        };
        $scope.sort = function (column) {
            if (column === $scope.currentSort) $scope.currentSortAscending = !$scope.currentSortAscending;
            $scope.currentSort = column;
            $scope.sortNodes();
        };
        $scope.nextPage = function () {
            if ($scope.currentPage < $scope.totalPages) {
                $scope.currentPage++;
                $scope.filterNodes();
            }
        };
        $scope.prevPage = function () {
            if ($scope.currentPage > 1) {
                $scope.currentPage--;
                $scope.filterNodes();
            }
        };
        $scope.sortNodes = function () {
            var direction = $scope.currentSortAscending ? 1 : -1;
            var nodes = $scope.unlinkedNodes.sort((a, b) => {
                if (a[this.currentSort] < b[this.currentSort]) return -1 * direction;
                if (a[this.currentSort] > b[this.currentSort]) return direction;
                return 0;
            });
            $scope.sortedNodes = nodes;

            $scope.filterNodes();
        };
        $scope.filterNodes = function () {
            var first = ($scope.currentPage - 1) * $scope.pageSize;
            var last = ($scope.currentPage * $scope.pageSize) - 1;
            $scope.hasNext = $scope.currentPage < $scope.totalPages;
            $scope.hasPrevious = $scope.currentPage > 1;

            var nodes = $scope.sortedNodes.filter((row, index) => {
                return index >= first && index <= last;
            });

            $scope.filteredNodes = nodes;
        };
        $scope.checkChanged = function (node) {
            $scope.checkedCount += node.checked ? 1 : -1;
            $scope.hasChecked = $scope.checkedCount > 0;
        }
        $scope.unpublishSelected = function () {
            $scope.refresh();
        }
        $scope.deleteSelected = function () {
            $scope.refresh();
        }
        $scope.executeMenuAction = function (action) {
            if (action.alias === 'export') return exportData();

            var mediaCnt = 0, contentCnt = 0, selected = [];
            for (var i = 0; i < $scope.unlinkedNodes.length; i++) {
                var node = $scope.unlinkedNodes[i];
                if (!node.checked) continue;
                selected.push(node.id);
                if (node.isMedia) mediaCnt++;
                else contentCnt++;
            }

            if (mediaCnt === 0 && contentCnt === 0) {
                notificationsService.error("No Nodes Selected", "You must select one or more nodes first");
            } else if (mediaCnt > 0 && contentCnt > 0) {
                notificationsService.error("Mixed Types", "You cannot perform an action on both media and content nodes at the same time");
            } else if (mediaCnt > 0 && (action.alias === 'publish' || action.alias === 'unpublish')) {
                notificationsService.error("Media Selected", "You cannot publish or unpublish media nodes");
            } else {
                dialogService.open({
                    template: '/App_Plugins/RelatedNodes/Modal.html',
                    callback: function (result) {
                        if (result) {
                            $scope.refresh();
                        }
                    },
                    show: true,
                    dialogData: {
                        action: action,
                        isMedia: mediaCnt > 0,
                        selected: selected
                    }
                });
            }
        }
        $scope.excludedFoldersChanged = function (result) {
            if (!result || !result.success) return;

            if (result.isMedia) {
                $scope.excludedMediaFolders = result.nodeIds || result.NodeIds || '';
            } else {
                $scope.excludedContentFolders = result.nodeIds || result.NodeIds || '';
            }
            $scope.refresh();
        }
        $scope.changeExcludedFolders = function (isMedia) {
            dialogService.open({
                callback: $scope.excludedFoldersChanged,
                show: true,
                template: '/App_Plugins/RelatedNodes/ExcludedFolder.html',
                dialogData: {
                    isMedia: isMedia,
                    values: isMedia ? $scope.excludedMediaFolders : $scope.excludedContentFolders
                }
            });
        }

        var exportData = function () {
            var csv = buildCsv();
            var blob = new Blob([csv], { type: 'text/csv' });
            var link = document.createElement('a');

            link.download = "OrphanedNodes.csv";
            link.href = URL.createObjectURL(blob);
            link.dataset.downloadurl = ['text/csv', link.download, link.href].join(':');
            link.target = '_blank';

            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            setTimeout(function () { URL.revokeObjectURL(link.href); }, 5000);
        }

        var buildCsv = function () {
            var nodes = $scope.unlinkedNodes;
            var csv = ['Id,Name,Link,Path,Last Edited,Content Type,Status,Is Media\r\n'];

            for (var i = 0; i < nodes.length; i++) {
                var node = nodes[i];
                csv.push(node.id);
                csv.push(',"');
                csv.push((('' + node.name) || '').replace(/"/g, '""'));
                csv.push('","');
                csv.push((('' + node.link) || '').replace(/"/g, '""'));
                csv.push('","');
                csv.push((('' + node.pathName) || '').replace(/"/g, '""'));
                csv.push('","');
                csv.push((('' + node.lastEditDate) || '').replace(/"/g, '""'));
                csv.push('","');
                csv.push((('' + node.contentTypeAlias) || '').replace(/"/g, '""'));
                csv.push('","');
                csv.push((('' + node.status) || '').replace(/"/g, '""'));
                csv.push('",');
                csv.push(node.isMedia);
                csv.push('\r\n');
            }

            return csv.join('');
        }

        $scope.refresh();
    }]);
