﻿angular.module('umbraco').controller('Our.Umbraco.RelatedNodes.RelationsController', ['$scope', 'editorState', 'Our.Umbraco.RelatedNodes.Service', 'notificationsService',
    function ($scope, editorState, relatedNodesService, notificationsService) {
        $scope.model.hideLabel = true;
        $scope.loading = true;
        $scope.currentSort = {
            incoming: { column: "name", ascending: true },
            outgoing: { column: "name", ascending: true }
        };
        $scope.incomingNodeCount = 0;
        $scope.outgoingNodeCount = 0;
        $scope.incomingNodes = [];
        $scope.outgoingNodes = [];
        $scope.sortedIncomingNodes = [];
        $scope.sortedOutgoingNodes = [];

        var sortNodes = function (nodes, currentSort) {
            var direction = currentSort.ascending ? 1 : -1;
            var sortedNodes = nodes.sort((a, b) => {
                if (a[this.currentSort] < b[this.currentSort]) return -1 * direction;
                if (a[this.currentSort] > b[this.currentSort]) return direction;
                return 0;
            });

            return sortedNodes;
        };
        var resort = function (sortIncoming, sortOutgoing) {
            if (sortIncoming) $scope.sortedIncomingNodes = sortNodes($scope.incomingNodes, $scope.currentSort.incoming);
            if (sortOutgoing) $scope.sortedOutgoingNodes = sortNodes($scope.outgoingNodes, $scope.currentSort.outgoing);
        };
        $scope.sort = function (direction, column) {
            var currentSort = $scope.currentSort[direction];
            if (!currentSort) return;

            if (column === currentSort.column) currentSort.ascending = !currentSort.ascending;
            currentSort.column = column;
            resort(direction === 'incoming', direction === 'outgoing');
        };

        relatedNodesService.getLinkedNodes(editorState.current.id)
            .then(
            function successCallback(response) {
                $scope.loading = false;
                if (response.error) {
                    console.log("An error occurred while trying to process the API call - " + JSON.stringify(response));
                    notificationsService.error('Refresh Error', 'Unable to load the links');
                    return;
                }

                var data = response.data || response.Data;
                $scope.incomingNodeCount = data.incomingNodeCount || data.IncomingNodeCount;
				$scope.outgoingNodeCount = data.outgoingNodeCount || data.OutgoingNodeCount;
                $scope.incomingNodes = (data.incomingNodes || data.IncomingNodes || []).map(x => new LinkedNodeData(x));
                $scope.outgoingNodes = (data.outgoingNodes || data.OutgoingNodes || []).map(x => new LinkedNodeData(x));

                resort(true, true);
			},
			function errorCallback(response) {
			    $scope.loading = false;
			    console.log("An error occurred while trying to process the API call - " + JSON.stringify(error));
			    notificationsService.error('Refresh Error', 'Unable to load the links');
			});
	}]);
