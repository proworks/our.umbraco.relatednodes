﻿angular.module('umbraco').controller('Our.Umbraco.RelatedNodes.ExcludedFolderController', ['$scope',
    function ($scope) {
        var options = $scope.dialogData || {};
        var isMedia = options.isMedia ? true : false;
        var section = isMedia ? 'media' : 'content';
        var values = options.values || '';

        $scope.pickerView = 'views/propertyeditors/' + section + 'picker/' + section + 'picker.html';
        $scope.model = {
            value: values,
            config: {
                multiPicker: '1',
                startNode: {
                    query: "",
                    type: section,
                    id: -1
                }
            }
        };

        $scope.saveNodes = function () {
            $scope.submit({
                success: true,
                isMedia: isMedia,
                nodeIds: $scope.model.value
            });
        }
    }]);
