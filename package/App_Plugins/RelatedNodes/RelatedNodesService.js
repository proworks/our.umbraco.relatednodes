﻿angular.module('umbraco.resources').factory('Our.Umbraco.RelatedNodes.Service', ['$q', '$http',
    function ($q, $http) {

        var serviceRoot = '/umbraco/backoffice/api/RelatedNodes/';

        return {
            getUnlinkedNodeData: function (excludedNodePaths, includeNodesWithOnlyOutgoingLinks) {
                return $http.get(serviceRoot + 'GetUnlinkedNodeData?includeNodesWithOnlyOutgoingLinks=' + (includeNodesWithOnlyOutgoingLinks ? 'true' : 'false') + '&excludedNodePaths=' + (excludedNodePaths ? encodeURIComponent(excludedNodePaths) : ''));
            },
            bulkAction: function (action, isMedia, selected, destinationId) {
                var selectedIds = (selected || []).join(',');
                return $http.post(serviceRoot + 'BulkAction?action=' + (action ? encodeURIComponent(action) : '') + '&isMedia=' + (isMedia ? 'true' : 'false') + '&selectedNodeIds=' + (selectedIds ? encodeURIComponent(selectedIds) : '') + '&destinationId=' + (destinationId ? encodeURIComponent(destinationId) : ''));
            },
            getLinkedNodes: function (contentId) {
                return $http.get(serviceRoot + 'GetLinkedNodes?contentId=' + (contentId ? encodeURIComponent(contentId) : ''));
            }
        }
    }]);

function LinkedNodeData(data) {
    var me = this;

    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            var lowerKey = key[0].toLowerCase() + key.substring(1);
            me[lowerKey] = data[key];
        }
    }

    if (me.pathElements && me.pathElements.length) {
        var link = [], pathName = [];
        for (var i = 0; i < me.pathElements.length; i++) {
            var pe = me.pathElements[i];
            var id = pe.id || pe.Id;
            var name = pe.name || pe.Name;
            if (id && name) {
                if (link.length > 0) {
                    link.push(' &gt; ');
                }
                link.push('<a href="#/');
                link.push(me.isMedia ? 'media/media' : 'content/content');
                link.push('/edit/');
                link.push(id);
                link.push('">');
                link.push($('<div/>').text(name).html());
                pathName.push(name);
                link.push('</a>');
            }
        }
        me.linkedPath = link.join('');
        me.pathName = pathName.join(' > ');
    } else me.linkedPath = '';

    if (me.lastEditDate) {
        me.lastEditDate = moment(me.lastEditDate).format('YYYY-MM-DD HH:mm:ss');
    }

    me.status = me.isMedia ? 'Media' : ((me.isPublished ? 'P' : 'Unp') + 'ublished Content');
    me.link = '#/' + (me.isMedia ? 'media/media' : 'content/content') + '/edit/' + me.id;
    me.checked = false;
}