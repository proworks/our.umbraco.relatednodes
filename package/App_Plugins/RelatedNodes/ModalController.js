﻿angular.module('umbraco').controller('Our.Umbraco.RelatedNodes.ModalController', ['$scope', '$http', 'Our.Umbraco.RelatedNodes.Service', 'notificationsService', 'dialogService',
    function ($scope, $http, relatedNodesService, notificationsService, dialogService) {
        $scope.action = $scope.dialogData.action;
        $scope.selected = $scope.dialogData.selected;
        $scope.isMedia = $scope.dialogData.isMedia;
        $scope.nodeType = $scope.isMedia ? 'media' : 'content';
        $scope.needsDestination = $scope.action.alias === 'move';
        $scope.neededDestination = $scope.dialogData.action.alias === 'move';
        $scope.nodeCount = $scope.selected.length;
        $scope.single = $scope.selected.length === 1;
        $scope.success = false;
        $scope.busy = false;
        $scope.error = null;
        $scope.destination = null;

        $scope.selectDestination = function () {
            var opts = { callback: $scope.nodePicked, show: true };

            if ($scope.isMedia) {
                opts.template = 'views/common/dialogs/mediaPicker.html';
            } else {
                opts.template = 'views/common/dialogs/treePicker.html';
                opts.treeAlias = 'content';
                opts.section = 'content';
            }

            dialogService.open(opts);
        }
        $scope.completeAction = function () {
            if ($scope.needsDestination) return;

            $scope.busy = true;
            var destId = $scope.destination ? $scope.destination.id : null;
            relatedNodesService.bulkAction($scope.action.alias, $scope.isMedia, $scope.selected, destId)
                .then(
                    function (data) {
                        $scope.busy = false;
                        if (data.error) {
                            console.log("An error occurred while trying to process the API call - " + JSON.stringify(data));
                            notificationsService.error($scope.action.name + ' Error', data.error);
                            return;
                        }

                        $scope.success = data.data ? true : false;
                        $scope.submit($scope.success);
                    },
                    function (error) {
                        $scope.busy = false;
                        console.log("An error occurred while trying to process the API call - " + JSON.stringify(error));
                        notificationsService.error($scope.action.name + ' Error', error.data.message);
                    }
                );
        }
        $scope.nodePicked = function (pickedItems) {
            var pickedItem = pickedItems && pickedItems.length ? pickedItems[0] : pickedItems;

            if (pickedItem) {
                $scope.destination = pickedItem;
                $scope.needsDestination = false;
            } else if ($scope.neededDestination) {
                $scope.needsDestination = true;
            }
        }
    }]);
