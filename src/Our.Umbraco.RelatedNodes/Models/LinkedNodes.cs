﻿using System.Collections.Generic;

namespace Our.Umbraco.RelatedNodes.Models
{
    public class LinkedNodes
    {
        public int IncomingNodeCount { get; set; }
        public int OutgoingNodeCount { get; set; }

        public IEnumerable<LinkedNodeData> IncomingNodes { get; set; }
        public IEnumerable<LinkedNodeData> OutgoingNodes { get; set; }
    }
}
