﻿using System;
using System.Collections.Generic;

namespace Our.Umbraco.RelatedNodes.Models
{
    public class LinkedNodeData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContentTypeAlias { get; set; }
        public string ContentTypeName { get; set; }
        public string ContentTypeIcon { get; set; }
        public int ContentTypeId { get; set; }
        public DateTime LastEditDate { get; set; }
        public string Path { get; set; }
        public IEnumerable<PathElement> PathElements { get; set; }
        public bool IsMedia { get; set; }
        public bool IsPublished { get; set; }
        public string LinkDirection { get; set; }
    }

    public class PathElement
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
