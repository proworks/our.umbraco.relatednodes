﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security;
using System.Text;
using System.Xml;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Web.UI.JavaScript;

namespace Our.Umbraco.RelatedNodes.EventHandlers
{
    public class ServerVariableAdder : ApplicationEventHandler
    {
        private const string SimpleQuery = "SELECT id [Id], uniqueID [Key] FROM umbracoNode WHERE nodeObjectType = @0 AND trashed = 0 AND [path] LIKE '-1,%'";
        private const string NodeQuery = "SELECT id [Id], parentID [ParentId], uniqueID [Key], [text] [Name], ct.alias [ContentType] FROM umbracoNode n JOIN cmsContent c ON n.id = c.nodeId JOIN cmsContentType ct ON c.contentType = ct.nodeId WHERE nodeObjectType = @0 AND trashed = 0 AND [path] LIKE '-1,%'";
        private const string ContentObjectType = "C66BA18E-EAF3-4CFF-8A22-41B16D66A972";
        private const string MediaObjectType = "B796F64C-1F99-4FFB-B886-4BF4BC011A9C";

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ServerVariablesParser.Parsing += (s, e) => AddVariables(s, e, applicationContext);
            base.ApplicationStarted(umbracoApplication, applicationContext);
        }

        private static void AddVariables(object sender, Dictionary<string, object> e, ApplicationContext context)
        {
            var excludedContents = ConfigurationManager.AppSettings["RelatedNodes:ExcludedContentFolderIds"]?.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries) ?? new string[0];
            var excludedMedias = ConfigurationManager.AppSettings["RelatedNodes:ExcludedMediaFolderIds"]?.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries) ?? new string[0];

            if (excludedContents.Length > 0 || excludedMedias.Length > 0)
            {
                var needsGuids = excludedContents.Any(i => Guid.TryParse(i, out _)) || excludedMedias.Any(i => Guid.TryParse(i, out _));
                var needsNodes = excludedContents.Any(i => !int.TryParse(i, out _) && !Guid.TryParse(i, out _)) || excludedMedias.Any(i => !int.TryParse(i, out _) && !Guid.TryParse(i, out _));
                var dataMaps = GetDataMaps(context, needsGuids,  needsNodes);
                excludedContents = Remap(excludedContents, dataMaps.Content, context);
                excludedMedias = Remap(excludedMedias, dataMaps.Media, context);
            }

            var map = new Dictionary<string, object>
            {
                ["DefaultIncludeNodesWithOnlyOutgoingLinks"] = bool.TryParse(ConfigurationManager.AppSettings["RelatedNodes:IncludeNodesWithOnlyOutgoingLinks"], out var b) && b,
                ["DefaultExcludedContentFolderIds"] = string.Join(",", excludedContents),
                ["DefaultExcludedMediaFolderIds"] = string.Join(",", excludedMedias)
            };

            if (!e.Keys.Contains("RelatedNodes")) e.Add("RelatedNodes", map);
        }

        private static DataMaps GetDataMaps(ApplicationContext context, bool needsGuids, bool needsNodes)
        {
            var maps = new DataMaps { Content = new DataMap(), Media = new DataMap() };

            if (needsGuids || needsNodes)
            {
                LoadDataMap(context, ContentObjectType, needsNodes, maps.Content);
                LoadDataMap(context, MediaObjectType, needsNodes, maps.Media);
            }

            return maps;
        }

        private static void LoadDataMap(ApplicationContext context, string objectType, bool needsNodes, DataMap map)
        {
            var nodes = context.DatabaseContext.Database.Fetch<Node>(needsNodes ? NodeQuery : SimpleQuery, objectType);
            map.KeyMap = new Dictionary<Guid, int>(nodes.Count);
            nodes.ForEach(n => map.KeyMap[n.Key] = n.Id);

            if (!needsNodes) return;

            map.Nodes = new XmlDocument();

            try
            {
                var childNodeLookup = nodes.ToLookup(n => n.ParentId);
                var sb = new StringBuilder(nodes.Count * 50);
                sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<root>");
                AddChildNodes(sb, childNodeLookup[-1], childNodeLookup);
                sb.Append("</root>");

                map.Nodes.LoadXml(sb.ToString());
            }
            catch (Exception ex)
            {
                context.ProfilingLogger.Logger.Error<ServerVariableAdder>("Could not load the XML document", ex);
            }
        }

        private static void AddChildNodes(StringBuilder sb, IEnumerable<Node> childNodes, ILookup<int, Node> childNodeLookup)
        {
            foreach (var node in childNodes)
            {
                sb.Append("<").Append(node.ContentType);
                sb.Append(" id=\"").Append(node.Id);
                sb.Append("\" name=\"").Append(SecurityElement.Escape(node.Name));
                sb.Append("\" key=\"").Append(node.Key);
                sb.Append("\">");

                AddChildNodes(sb, childNodeLookup[node.Id], childNodeLookup);

                sb.Append("</").Append(node.ContentType).Append(">");
            }
        }

        private static string[] Remap(string[] values, DataMap map, ApplicationContext context)
        {
            var output = new List<string>(values.Length);

            foreach (var value in values)
            {
                output.AddRange(Remap(value, map, context));
            }

            return output.ToArray();
        }

        private static IEnumerable<string> Remap(string value, DataMap map, ApplicationContext context)
        {
            if (int.TryParse(value, out _)) return new[] { value };
            if (Guid.TryParse(value, out var key)) return map.KeyMap.TryGetValue(key, out var keyId) ? new[] { keyId.ToString() } : new string[0];

            try
            {
                var ids = new List<string>();
                foreach (var node in map.Nodes.SelectNodes(value))
                {
                    if (!(node is XmlElement elem)) continue;

                    var id = elem.GetAttribute("id");
                    if (int.TryParse(id, out _)) ids.Add(id);
                }

                return ids;
            }
            catch (Exception ex)
            {
                context.ProfilingLogger.Logger.Error<ServerVariableAdder>("Could not select nodes based on XPath - " + value, ex);
            }

            return new string[0];
        }

        private class DataMaps
        {
            public DataMap Content { get; set; }
            public DataMap Media { get; set; }
        }
        private class DataMap
        {
            public Dictionary<Guid, int> KeyMap { get; set; }
            public XmlDocument Nodes { get; set; }
        }
        private class Node
        {
            public int Id { get; set; }
            public int ParentId { get; set; }
            public Guid Key { get; set; }
            public string Name { get; set; }
            public string ContentType { get; set; }
        }
    }
}
