﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Our.Umbraco.RelatedNodes.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace Our.Umbraco.RelatedNodes.Controllers
{
    public class RelatedNodesController : UmbracoAuthorizedApiController
    {
        private const string BaseNodeSelect = "SELECT n.id Id, n.text Name, ct.alias ContentTypeAlias, cn.text ContentTypeName, ct.icon ContentTypeIcon, MAX(cv.VersionDate) LastEditDate, CAST(CASE n.nodeObjectType WHEN 'C66BA18E-EAF3-4CFF-8A22-41B16D66A972' THEN 0 ELSE 1 END AS BIT) IsMedia, CASE WHEN n.path = '-1,' + CAST(n.id AS nvarchar) THEN '' ELSE SUBSTRING(n.path, 4, LEN(n.path) - 4 - LEN(CAST(n.id AS nvarchar))) END Path, CAST(MIN(CAST(COALESCE(d.published, 0) AS INT)) AS BIT) IsPublished ";
        private const string BaseNodeJoin = "    JOIN cmsContent c ON n.id = c.nodeId " +
                                            "    JOIN cmsContentType ct ON c.contentType = ct.nodeId " +
                                            "    JOIN cmsContentVersion cv ON n.id = cv.ContentId " +
                                            "    JOIN umbracoNode cn ON ct.nodeId = cn.id " +
                                            "    LEFT JOIN cmsDocument d ON n.id = d.nodeId AND cv.VersionId = d.versionId ";
        private const string BaseNodeWhere = "WHERE n.trashed = 0 AND NOT n.path LIKE '-2%' " +
                                             "    AND n.nodeObjectType IN ('C66BA18E-EAF3-4CFF-8A22-41B16D66A972', 'B796F64C-1F99-4FFB-B886-4BF4BC011A9C') " +
                                             "    AND COALESCE(d.newest, 1) = 1 ";
        private const string BaseNodeGroup = "GROUP BY n.id, n.text, ct.alias, cn.text, ct.icon, n.nodeObjectType, n.path";

        private const string RelatedNodeQuery = BaseNodeSelect + ", 'Incoming' LinkDirection " +
                                                "FROM umbracoRelation ur " +
                                                "    JOIN umbracoNode n ON ur.parentId = n.id " +
                                                BaseNodeJoin +
                                                BaseNodeWhere +
                                                "    AND ur.childId = {0} " +
                                                BaseNodeGroup +
                                                " UNION ALL " +
                                                BaseNodeSelect + ", 'Outgoing' LinkDirection " +
                                                "FROM umbracoRelation ur " +
                                                "    JOIN umbracoNode n ON ur.childId = n.id " +
                                                BaseNodeJoin +
                                                BaseNodeWhere +
                                                "    AND ur.parentId = {0} " +
                                                BaseNodeGroup;

        private const string UnrelatedNodeQuery = BaseNodeSelect +
                                                  "FROM " +
                                                  "    umbracoNode n " +
                                                  BaseNodeJoin +
                                                  "    LEFT JOIN umbracoRelation pr ON n.id = pr.parentId AND pr.relType IN (SELECT rt.id FROM umbracoRelationType rt WHERE rt.alias IN ('nexuDocumentToDocument', 'nexuDocumentToMedia')) " +
                                                  "    LEFT JOIN umbracoRelation cr ON n.id = cr.childId AND cr.relType IN (SELECT rt.id FROM umbracoRelationType rt WHERE rt.alias IN ('nexuDocumentToDocument', 'nexuDocumentToMedia')) " +
                                                  BaseNodeWhere +
                                                  "    AND cr.id IS NULL " +
                                                  "    AND ({0} = 1 OR pr.id IS NULL) " +
                                                  BaseNodeGroup;

        /// <summary>
        /// Gets incoming links for a document
        /// </summary>
        /// <param name="contentId">
        /// The content id.
        /// </param>
        /// <returns>
        /// The <see cref="LinkedNodes"/>.
        /// </returns>
        public LinkedNodes GetLinkedNodes(int contentId)
        {
            var linkedNodes = ApplicationContext.DatabaseContext.Database.Fetch<LinkedNodeData>(string.Format(RelatedNodeQuery, contentId));
            SetPathElements(linkedNodes);

            var incomingLinks = linkedNodes.Where(n => n.LinkDirection == "Incoming").ToList();
            var outgoingLinks = linkedNodes.Where(n => n.LinkDirection == "Outgoing").ToList();

            return new LinkedNodes
            {
                IncomingNodeCount = incomingLinks.Count,
                IncomingNodes = incomingLinks,
                OutgoingNodeCount = outgoingLinks.Count,
                OutgoingNodes = outgoingLinks
            };
        }

        /// <summary>
        /// Gets all nodes that aren't related to other nodes
        /// </summary>
        /// <param name="excludedNodePaths">A comma-separated list of node paths to not include</param>
        /// <param name="includeNodesWithOnlyOutgoingLinks">Whether or not to exclude a node that only has outgoing relations</param>
        /// <returns>The list of <see cref="LinkedNodeData"/>.</returns>
        public IEnumerable<LinkedNodeData> GetUnlinkedNodeData(string excludedNodePaths, bool includeNodesWithOnlyOutgoingLinks)
        {
            var unlinkedNodes = ApplicationContext.DatabaseContext.Database.Fetch<LinkedNodeData>(string.Format(UnrelatedNodeQuery, includeNodesWithOnlyOutgoingLinks ? "1" : "0"));
            var excludedNodeIds = new HashSet<int>(excludedNodePaths?.Split(',').Select(p => p.Contains(',') ? p.Substring(p.LastIndexOf(',') + 1) : p).Select(p => int.TryParse(p.Trim(), out var i) ? i : 0).Where(i => i != 0) ?? new int[0]);
            var excludedPaths = excludedNodeIds.Select(p => new Regex($"(^|\\b){p}(\\b|$)", RegexOptions.Compiled)).ToList();

            if (excludedPaths.Any())
            {
                unlinkedNodes = unlinkedNodes.Where(n => !excludedNodeIds.Contains(n.Id) && (string.IsNullOrEmpty(n.Path) || !excludedPaths.Any(p => p.IsMatch(n.Path)))).ToList();
            }

            SetPathElements(unlinkedNodes);

            return unlinkedNodes;
        }

        private void SetPathElements(List<LinkedNodeData> nodes)
        {
            var knownNodeNames = new Dictionary<int, string>(nodes.Count * 3);
            var allNeededIds = new HashSet<int>();

            foreach (var node in nodes)
            {
                knownNodeNames[node.Id] = node.Name;
                if (!string.IsNullOrWhiteSpace(node.Path)) allNeededIds.UnionWith(node.Path.Split(',').Select(p => int.TryParse(p, out var id) ? id : 0).Where(i => i != 0));
            }

            AddKnownNodeNames(knownNodeNames, allNeededIds);
            nodes.ForEach(n => GeneratePathElements(n, knownNodeNames));
        }

        private const int BatchSize = 500;
        private const string NodeNameQuery = "SELECT n.id [Id], n.text [Name] FROM umbracoNode n WHERE n.id IN ({0})";
        private void AddKnownNodeNames(Dictionary<int, string> knownNodeNames, HashSet<int> allNeededIds)
        {
            allNeededIds.ExceptWith(knownNodeNames.Keys);
            if (allNeededIds.Count == 0) return;

            var neededIds = allNeededIds.ToList();
            var start = 0;
            while (start < neededIds.Count)
            {
                var count = neededIds.Count < (start + BatchSize) ? neededIds.Count - start : BatchSize;
                var ids = neededIds.GetRange(start, count);
                var nodeNames = ApplicationContext.DatabaseContext.Database.Fetch<NodeName>(string.Format(NodeNameQuery, string.Join(",", ids)));
                nodeNames.ForEach(n => knownNodeNames[n.Id] = n.Name);

                start += BatchSize;
            }
        }
        private class NodeName
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        private void GeneratePathElements(LinkedNodeData node, Dictionary<int, string> nodeNames)
        {
            if (string.IsNullOrEmpty(node.Path))
            {
                node.PathElements = new PathElement[0];
            }

            var elements = new List<PathElement>();
            foreach (var piece in node.Path.Split(','))
            {
                if (!int.TryParse(piece, out var id) || !nodeNames.TryGetValue(id, out var name) || name == null) continue;
                elements.Add(new PathElement { Id = id, Name = name });
            }

            node.PathElements = elements;
        }

        public bool BulkAction(string action, bool isMedia, string selectedNodeIds, string destinationId)
        {
            var nodeIds = (selectedNodeIds ?? string.Empty).Split(',').Select(s => int.TryParse(s, out var i) ? i : 0).Where(i => i > 0).ToList();
            if (nodeIds.Count == 0) return false;

            var cs = Services.ContentService;
            var ms = Services.MediaService;

            switch (action)
            {
                case "move":
                    if (!int.TryParse(destinationId, out var destId)) return false;

                    if (isMedia)
                    {
                        if (ms.GetById(destId) == null) return false;
                        ProcessNodes(nodeIds, i => ms.GetById(i), n => n.ParentId != destId, n => ms.Move(n, destId));
                    }
                    else
                    {
                        if (cs.GetById(destId) == null) return false;
                        ProcessNodes(nodeIds, i => cs.GetById(i), n => n.ParentId != destId, n => cs.Move(n, destId));
                    }

                    return true;
                case "publish":
                    ProcessNodes(nodeIds, i => cs.GetById(i), n => !n.Published, n => cs.Publish(n));
                    return true;
                case "unpublish":
                    ProcessNodes(nodeIds, i => cs.GetById(i), n => n.Published, n => cs.UnPublish(n));
                    return true;
                case "delete":
                    if (isMedia)
                    {
                        ProcessNodes(nodeIds, i => ms.GetById(i), n => !n.Trashed, n => ms.Delete(n));
                    }
                    else
                    {
                        ProcessNodes(nodeIds, i => cs.GetById(i), n => !n.Trashed, n => cs.Delete(n));
                    }
                    return true;
                default:
                    return false;
            }
        }

        private void ProcessNodes<T>(List<int> nodeIds, Func<int, T> getNode, Func<T, bool> check, Action<T> handleNode)
        {
            nodeIds.ForEach(i =>
            {
                var node = getNode(i);
                if (node == null || !check(node)) return;
                handleNode(node);
            });
        }
    }
}
